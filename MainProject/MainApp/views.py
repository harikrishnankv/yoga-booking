from django.shortcuts import render,redirect
from .models import *
from django.http.response import HttpResponse
from django.contrib.auth import authenticate,login,logout
from django.core.files.storage import FileSystemStorage
# Create your views here.
def addcustomer(request):
    if request.method=='POST':
        f=request.POST.get('name')
        dob=request.POST.get('dob')
        ge=request.POST.get('gender')
        em=request.POST.get('email')
        ph=request.POST.get('phone')
        un=request.POST.get('uname')
        psw=request.POST.get('psswd')
        User.objects.create_user(first_name=f,dob=dob,gen=ge,email=em,phone=ph,username=un,password=psw)
        return render(request,"user/log.html")
    return render(request,"user/reg.html")
def new(request):
    k=category.objects.all()
    j=category.objects.all().values() 
    return render(request,"user/index.html",{"data":k})
def login_user(request):
    if request.method=='POST':
        em=request.POST.get('email')
        pw=request.POST.get('password')
        user=authenticate(request,email=em,password=pw)
        if user:
            login(request,user)
            return render(request,"user/index.html")
        else:
            return HttpResponse("Invalid User!!")
    return render(request,"user/log.html")

def dashboard1(request):
        return render(request,"dashboard/index1.html")


def addcategory(request):
    if request.method=='POST':
        c=request.POST.get('cname')
        n=request.POST.get('noc')
        v=request.POST.get('valid')
        pa=request.POST.get('paid')
        pr=request.POST.get('price')
        im=request.FILES['img']
        fo=FileSystemStorage()
        im2=fo.save(im.name,im)
        category.objects.create(cname=c,noc=n,valid=v,paid=pa,price=pr,image=im2)
       
    return render(request,'dashboard/cate.html')

def adpt(request):
    return render(request,"dashboard/dept.html")

def addcate(request):
    return render(request,"dashboard/cate.html")

def addoct(request):
    k=department.objects.all()
    if request.method=='POST':
        d=request.POST.get('drname')
        m=request.POST.get('department')
        m1=department.objects.get(id=m)
        doctor.objects.create(drname=d,dep=m1)
    return render(request,"dashboard/dr.html",{"data":k})

def adddept(request):
    if request.method=='POST':
        dn=request.POST.get('dname')
        department.objects.create(dname=dn)
    return render(request,"dashboard/dept.html")

def addpro(request):
    return render(request,"dashboard/pro.html")

def addprogram(request):
    k=category.objects.all()
    if request.method=='POST':
        t=request.POST.get('title')
        cl=request.POST.get('classes')
        vi=request.POST.get('videourl')
        im=request.FILES['image']
        fo=FileSystemStorage()
        im2=fo.save(im.name,im)
        v=request.FILES['video']
        fo2=FileSystemStorage()
        v2=fo2.save(v.name,v)
        c=request.POST.get('category')
        c1=category.objects.get(id=c)
        program.objects.create(title=t,classes=cl,videourl=vi,image=im2,cu=c1,videofile=v2)
    return render(request,"dashboard/pro.html",{"data":k})      

def getuser(request):
    k=doctor.objects.all()
    if request.method=="POST":
        se=request.POST.get('name')
        res=doctor.objects.filter(name=se)
        return render(request,"dashboard/tble.html",{"data":res})
    return render(request,"dashboard/tble.html",{"data":k})

def deluser(request,userid):
    x=doctor.objects.get(id=userid)
    x.delete()
    return redirect("tble")

def update_user(request,userid):
    x=doctor.objects.filter(id=userid).values()
    k=department.objects.all()
    if request.method=="POST":
        F=request.POST.get('drname')
        m=request.POST.get('department')
        m1=department.objects.get(id=m)
        x.update(drname=F,dep=m1)
        return redirect("tble")
    return render(request,"dashboard/update1.html",{"userdata":x[0],"id":userid,"data":k})

def getprogram(request):
    l=program.objects.all()
    if request.method=="POST":
        sl=request.POST.get('title')
        re=program.objects.filter(title=sl)
        return render(request,"dashboard/tblpro.html",{"data":re})
    return render(request,"dashboard/tblpro.html",{"data":l})


def delpro(request,userid):
    x=program.objects.get(id=userid)
    x.delete()
    return redirect("tble2") 
    
def update_pro(request,userid):
    u=program.objects.filter(id=userid).values()
    a=category.objects.all()
    if request.method=="POST":
        f=request.POST.get('title')
        p=request.POST.get('classes')
        i=request.POST.get('videourl')
        im=request.FILES['image']
        fo=FileSystemStorage()
        im2=fo.save(im.name,im)
        c=request.POST.get('category')
        c2=category.objects.get(id=c)
        u.update(title=f,classes=p,videourl=i,image=im2,cu=c2)
        return redirect("tble2")
    return render(request,"dashboard/update2.html",{"userdata":u[0],"id":userid,"data":a})


def getapt(request):
    h=appointment.objects.all()
    if request.method=="POST":
        dl=request.POST.get('atime')
        de=appointment.objects.filter(atime=de)
        return render(request,"dashboard/tblapt.html",{"data":de})
    return render(request,"dashboard/tblapt.html",{"data":h})


def getus(request):
    j=User.objects.all()
    if request.method=="POST":
        s3=request.POST.get('name')
        res=User.objects.filter(name=s3)
        return render(request,"dashboard/tblusers.html",{"data":res})
    return render(request,"dashboard/tblusers.html",{"data":j})

def lipro(request):
    k=category.objects.all()
    j=category.objects.all().values() 
    return render(request,"user/products.html",{"data":k})

def about(request):
   return render(request,"user/about.html")


def programmes(request):
   return render(request,"user/products.html")

def prgrmcats(request):
    l=category.objects.all()
    return render(request,"user/category.html",{'data':l})


def prgrms(request,catid):
    p=program.objects.filter(cu__id=catid)
    print("###########################")
    print(p)
    ca_details=category.objects.filter(id=catid).values()
    return render(request,"user/programs.html",{'data':p,'data2':ca_details[0]})


def prsb(request):
    if request.method=='POST':
        em=request.POST.get('email')
        pw=request.POST.get('password')
        user=authenticate(request,email=em,password=pw)
        if user:
            login(request,user)
            return render(request,"user/programs.html")
        else:
            return HttpResponse("Invalid User!!")
    return render(request,"user/log.html")


def contact(request):
   return render(request,"user/contact.html")


def price(request):
    d=category.objects.all()
    return render(request,"user/price.html",{'data':d})

def subscribe(request,catid,uid):
    if request.user.is_authenticated:
        p1=category.objects.filter(id=catid)
        u=User.objects.filter(id=uid).values()
        print("###########################")
        print(p1)
       

        return render(request,"user/subscribe.html",{'data2':p1})
    else:
        return redirect('l')

def app(request):
    
         return render(request,"user/appo.html")
    

def lout(request):
    logout(request)
    return redirect("l")

def admin_log(request):
   
        return render(request,"dashboard/adlog.html")
   
def app_details(request):
    w=department.objects.all()
    y=doctor.objects.all()
    if request.method=="POST":
        s1=request.POST.get('atime')
        s2=request.POST.get('department')
        s3=request.POST.get('email')
        s4=request.POST.get('phone')
        s5=request.POST.get('address')
        s6=request.POST.get('state')
        s7=request.POST.get('city')
        s8=request.POST.get('zipcode')
        s9=request.POST.get('message')
        s10=request.POST.get('date')
        order.objects.create(atime=s1,department=s2,email=s3,phone=s4,address=s5,state=s6,city=s7,zipcode=s8,message=s9,date=s10)
    return render(request,"user/appo.html",{"data":w,"dr":y})


    

