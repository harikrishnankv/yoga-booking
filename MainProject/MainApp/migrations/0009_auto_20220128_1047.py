# Generated by Django 3.2.9 on 2022-01-28 05:17

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('MainApp', '0008_category'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='dob',
            field=models.DateField(max_length=40, null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='gen',
            field=models.CharField(choices=[('Male', 'Male'), ('Female', 'Female')], max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='cus',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='program',
            name='cu',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='customer',
        ),
    ]
