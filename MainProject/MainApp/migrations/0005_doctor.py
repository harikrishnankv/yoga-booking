# Generated by Django 4.0 on 2022-01-27 05:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('MainApp', '0004_department'),
    ]

    operations = [
        migrations.CreateModel(
            name='doctor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('drname', models.CharField(max_length=40, null=True)),
                ('dep', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MainApp.department')),
            ],
        ),
    ]
