from django.contrib import admin
from .models import *

admin.site.register(User)
admin.site.register(department)
admin.site.register(doctor)
admin.site.register(appointment)
admin.site.register(program)
admin.site.register(category)
admin.site.register(order)