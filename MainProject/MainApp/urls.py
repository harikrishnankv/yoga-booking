from django.urls import path
from .views import*
from MainApp import views

urlpatterns = [
  
    path('home',views.addcustomer,name="add"),
    path('',views.new,name="h"),
    path('log',views.login_user,name="l"),
    path('d1',views.dashboard1,name="dh1"),
    path('adpt',views.adpt,name="adt"),
    path('addc',views.addcate,name="adc"),
    path('adct',views.addcategory,name='adtc'),
    path('adr',views.addoct,name="addr"),
    path('addept',views.adddept,name="adde"),
    path('addproi',views.addpro,name="adpr"),
    path('addprg',views.addprogram,name="adpg"),
    path('table',views. getuser,name="tble"),
    path('del<int:userid>',views.deluser,name="dl"),
    path('edit<int:userid>',views.update_user,name="edi"),
    path('table2',views.getprogram,name="tble2"),
    path('delproi<int:userid>',views.delpro,name="dlp"),
    path('editproi<int:userid>',views.update_pro,name="edp"),
    path('table3',views.getapt,name="tble3"),
    path('table4',views.getus,name="tble4"),
    path('lipr',views.lipro,name="listpro"),
    path('abo',views.about,name="about"),
    path('prd',views.prgrmcats,name="programmes"),
    path('prgms<int:catid>',views.prgrms,name="prog"),
    path('prs',views.prsb,name="prsb"),
    path('cnt',views.contact,name="contac"),
    path('rate',views.price,name="pricing"),
    path('sub<int:catid><int:uid>',views.subscribe,name="subscribe"),
    path('appo',views.app,name="appoin"),
    path('out',views.lout,name="logout"),
    path('admin_log',views.admin_log,name="adminlog"),
    path('addapp',views.app_details,name="addappointment"),
]   


