from django.db import models
from django.contrib.auth.models import AbstractUser,BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def _create_user(self, email, password, **other_fields):
        """
        Create and save a user with the given email and password. And any other fields, if specified.
        """
        if not email:
            raise ValueError('An Email address must be set')
        email = self.normalize_email(email)
        
        user = self.model(email=email, **other_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **other_fields):
        other_fields.setdefault('is_staff', False)
        other_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **other_fields)

    def create_superuser(self, email, password=None, **other_fields):
        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)

        if other_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if other_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **other_fields)
    

class User(AbstractUser):
    username=models.CharField(max_length=100,null=True,blank=True)
    first_name=models.CharField(max_length=100,null=True,blank=True)
    last_name=models.CharField(max_length=100,null=True,blank=True)
    email = models.EmailField(max_length=255, unique=True)
    phone = models.IntegerField(null=True)
    dob=models.DateField(max_length=40,null=True)
    gen=models.CharField(choices=(('Male','Male'),('Female','Female')),null=True,max_length=30)


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone']
    objects=UserManager()

    def get_username(self):
        return self.email 
  
class department(models.Model):
    dname=models.CharField(max_length=40,null=True)

    def __str__(self):
        return self.dname

class doctor(models.Model):
    drname=models.CharField(max_length=40,null=True)
    dep=models.ForeignKey('department', on_delete=models.CASCADE,null=True)

    def __str__(self):
        return self.drname

class appointment(models.Model):
    atime=models.TimeField(max_length=40,null=True)
    cus=models.OneToOneField('User', on_delete=models.CASCADE,null=True)
    dep=models.ForeignKey('department', on_delete=models.CASCADE,null=True)
    dr=models.ForeignKey('doctor', on_delete=models.CASCADE,null=True)
    email=models.EmailField(max_length=40,null=True)
    phone=models.IntegerField(null=True)
    address=models.CharField(max_length=40,null=True)
    state=models.CharField(max_length=40,null=True)
    city=models.CharField(max_length=40,null=True)
    zipcode=models.IntegerField(null=True)
    message=models.CharField(max_length=40,null=True)
    date=models.DateField(max_length=40,null=True)

    

    
class category(models.Model):
    cname=models.CharField(max_length=40,null=True)
    noc=models.IntegerField(null=True)
    valid=models.IntegerField(null=True)
    paid=models.BooleanField()
    price=models.IntegerField(null=True)
    image=models.ImageField(upload_to='images/',null=True)
    de=models.CharField(max_length=40,null=True)

    def __str__(self):
        return self.cname


class program(models.Model):
    title=models.CharField(max_length=40,null=True)
    classes=models.IntegerField(null=True)
    videourl=models.CharField(max_length=40,null=True)
    image=models.ImageField(upload_to='images/',null=True)
    cu=models.ForeignKey('category', on_delete=models.CASCADE,null=True)
    videofile=models.FileField(upload_to='videos/',null=True)

    def __str__(self):
        return self.title


class order(models.Model):
    us=models.ForeignKey('User', on_delete=models.CASCADE,null=True)
    time=models.TimeField(max_length=40,null=True)
    date=models.DateField(max_length=40,null=True)
    ct=models.ForeignKey('category', on_delete=models.CASCADE,null=True)
    paymentid=models.IntegerField(null=True)
    subscribe=models.BooleanField()
    amount=models.IntegerField(null=True)

def __str__(self):
        return self.ct
    